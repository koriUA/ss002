export default interface Dimension {
  brpObly: boolean;
  category: string;
  created: any;
  dataSubtype: string;
  dataType: string;
  dimRelations: any;
  displayDateFormat: any;
  displayName: string;
  displayedDimensionCategory: string;
  frequency: any;
  guid: string;
  id: string;
  includeProtectedData: boolean;
  inputSources: string[];
  isDateTime: any;
  metRelations: any;
  modified: any;
  name: string;
  orion1: boolean;
  orion2: boolean;
  owner: any;
  quickPicks: boolean;
  relations: any[];
  showAttributionMetrics: boolean;
  sortable: boolean;
  sourceDateFormat: any;
  valueWhiteList: any[];
};
