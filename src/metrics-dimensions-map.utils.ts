import {cloneDeep, every, has} from 'lodash';

import Metric from "./Metric";
import Dimension from "./Dimension";
import {MetricsByDimensionsMap} from "./MetricsByDimensionsMap";
import {MetricsMap} from "./MetricsMap";
import {DimensionsMap} from "./DimensionsMap";


export const isCustomMetric = (metric: Metric): boolean => {
    return metric.hasOwnProperty("isCustom") && (metric.isCustom ||
        (typeof metric.isCustom === "function" && metric.isCustom()))
}

export const getDimensionByGuid = (dimensions: Array<Dimension>, dimensionGuid: string): Dimension => {
    return cloneDeep(dimensions.find((dimension: Dimension) => {
        // "cme-dimension--" = 15
        return dimension.guid.substring(15) === dimensionGuid;
    }));
}

export const getStandardMetricByGuid = (metrics: Array<Metric>, metricGuid: string): Metric => {
    return cloneDeep(metrics.find((metric: Metric) => {
        // "cme-metric--" = 12
        return metric.guid.substring(12) === metricGuid;
    }));
}


export const intersect = (map1: MetricsByDimensionsMap | MetricsMap, map2: MetricsByDimensionsMap) => {
    const result = {};
    for (let [key, value] of Object.entries(map1)) {
        if (map2.hasOwnProperty(key)) {
            result[key] = value
        }
    }
    return result;
}

export const checkAllSelectedMetricsInSet = (
    unityMets: MetricsByDimensionsMap | MetricsMap,
    metricsMap: MetricsMap,
    selectedMetrics: Array<Metric>
): boolean => {
    return selectedMetrics.every((metric: Metric) => {
        if (metric.isCalc) {
            return metricsMap[metric.guid].fullDependencies.every(
                (metricGuid: string) => unityMets.hasOwnProperty(metricGuid)
            );
        }
        return unityMets.hasOwnProperty(metric.guid);
    });
}

export const getUnityMetsIfSelectedDimensions = (allSelDims: Array<Dimension>, standardMetrics: Array<Metric>, calculatedTest: boolean, metricsMap: MetricsMap): MetricsByDimensionsMap | MetricsMap => {
    let showAttrMetrics = true;
    // get available metrics by current dims selection
    // as intersection of what is available to all of dims at once
    let unityMets = {...allSelDims[0].metRelations}

    // as iterate through selected dimensions, check if there exists a dim that
    // does not allow attribution metrics to be shown
    if (!allSelDims[0].showAttributionMetrics) {
        showAttrMetrics = false;
    }

    for (let i = 1; i < allSelDims.length; i++) {
        unityMets = intersect(unityMets, allSelDims[i].metRelations);
        if (!allSelDims[i].showAttributionMetrics) {
            showAttrMetrics = false;
        }
    }
    if (standardMetrics.length > 0 && calculatedTest) {
        // check if all metrics are in the list of available metrics.
        // if some is not, disallow corrupted case. This also is used
        // by "calculated metrics checking" call if it is allowed to include
        const metricsInSet = checkAllSelectedMetricsInSet(
            unityMets,
            metricsMap,
            standardMetrics
        );
        unityMets = metricsInSet ? unityMets : {};
    }
    return unityMets;
}

export const isMetricCompatibleWithDimension = (
    metric: Metric,
    metricsMap: MetricsMap,
    dimension: Dimension
) => {
    if (metric.isCalc) {
        return every(
            metricsMap[metric.guid].fullDependencies,
            (metricGuid: string) => has(dimension.metRelations, metricGuid)
        );
    }
    return has(dimension.metRelations, metric.guid);
}

export const ifDimensionSupportsMetrics = (
    standardMetrics: Array<Metric>,
    metricsMap: MetricsMap,
    dimension: Dimension
) => {
    return every(standardMetrics, (metric: Metric) =>
        isMetricCompatibleWithDimension(metric, metricsMap, dimension)
    );
}

export const getAllMetricsByDimensions = (dimensionsMap: DimensionsMap,
                                          metricsMap: MetricsMap, standardMetrics: Array<Metric>): MetricsByDimensionsMap => {
    if (standardMetrics.length === 0) {
        return {};
    }

    const dimensionsMapArray: Array<Dimension> = Object.keys(dimensionsMap).map(
        (dimensionKey: string) => dimensionsMap[dimensionKey]
    );

    return dimensionsMapArray.reduce(
        (accumulator: MetricsByDimensionsMap, dimension: Dimension) => {
            if (dimension.isDateTime) {
                return accumulator;
            }

            const includeAll: boolean = ifDimensionSupportsMetrics(
                standardMetrics,
                metricsMap,
                dimension
            );

            if (includeAll) {
                return {...accumulator, ...dimension.metRelations};
            }

            return accumulator;
        },
        {}
    );
}

export const getMappedMetrics = (metricsMap: MetricsMap): MetricsMap => {
    const filteredMetrics = {...metricsMap}
    for (let metricId in filteredMetrics) {

        if (filteredMetrics[metricId] &&
            filteredMetrics[metricId].relations &&
            filteredMetrics[metricId].relations.length === 0) {
            delete filteredMetrics[metricId];
        }
    }
    return filteredMetrics;
}

export const getUnityMetsIfNoSelectedDimensions = (standardMetrics: Array<Metric>,
                                                   dimensionsMap: DimensionsMap, metricsMap: MetricsMap): MetricsByDimensionsMap | MetricsMap => {
    if (standardMetrics.length > 0) {
        // We have only metrics selected:
        // for each dimension, allowed metrics list, that contains all selected metrics,
        // is added to result
        return getAllMetricsByDimensions(
            dimensionsMap,
            metricsMap,
            standardMetrics
        );
    }

    // return only metrics without relations;
    // don't need Object.assign because already have spred inside getMappedMetrics
    return getMappedMetrics(metricsMap);
}
