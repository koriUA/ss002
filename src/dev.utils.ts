const fs = require('fs');

export function saveDataToTheResultFile(data) {
    fs.writeFileSync(`${__dirname}/../result.json`, JSON.stringify(data, null, '    '));
}
