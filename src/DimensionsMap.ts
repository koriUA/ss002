import Dimension from './Dimension';

export interface DimensionsMap {
  [dimensionId: string]: Dimension;
}
