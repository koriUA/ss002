export interface MetricsByDimensionsMap {
  // [metricId]: [dimensionId, dimensionId, ...];
  [metricId: string]: string[];
}
