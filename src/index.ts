import Dimension from "./Dimension";
import Metric from "./Metric";
import {DimensionsMap} from "./DimensionsMap";
import {MetricsMap} from "./MetricsMap";
import {MetricsByDimensionsMap} from "./MetricsByDimensionsMap";
import {
    getDimensionByGuid,
    getStandardMetricByGuid, getUnityMetsIfNoSelectedDimensions,
    getUnityMetsIfSelectedDimensions,
    isCustomMetric
} from "./metrics-dimensions-map.utils";



export const getAvailableMetricsFromDtoD = (
    dimensions: Array<Dimension>,
    metrics: Array<Metric>,
    dimensionsMap: DimensionsMap,
    metricsMap: MetricsMap,
    selDims: Array<Dimension>,
    selectedMetrics: Array<Metric>,
    calculatedTest: boolean
): MetricsByDimensionsMap | MetricsMap => {
    // loop over selected metrics to extract ones built with attributes
    //    add them to selDims for processing relations

    const attributeMetrics: Array<Dimension> = [];
    const standardMetrics: Array<Metric> = [];

    selectedMetrics.forEach((metric: Metric) => {
        if (isCustomMetric(metric)) {
            if (metric.calculationLogic) {
                // If custom metric has "calculationLogic" is it built from an attribute (dimension)
                // get the dimension that is the basis for the custom attribute metric
                attributeMetrics.push(
                    getDimensionByGuid(dimensions, metric.metricComputation)
                );
            } else {
                standardMetrics.push(
                    getStandardMetricByGuid(metrics, metric.metricComputation)
                );
            }
        } else {
            standardMetrics.push(metric);
        }
    });

    // Add the attribute metrics array to the selected dimensions if necessary

    const allSelDims: Array<Dimension> = [...selDims, ...attributeMetrics];

    if (allSelDims.length > 0) {
        return getUnityMetsIfSelectedDimensions(
            allSelDims,
            standardMetrics,
            calculatedTest,
            metricsMap
        );
    }
    return getUnityMetsIfNoSelectedDimensions(
        standardMetrics,
        dimensionsMap,
        metricsMap
    );
}



console.log('test..........');


import {dimensions_mock} from '../mock/dimensions';
import {dimensionsMap_mock} from '../mock/dimensionsMap';
import {metricsMap_mock} from '../mock/metricsMap';
import {metrics_mock} from '../mock/metrics';

import {selDims1} from "../mock/selDims";
import {selectedMetrics1} from "../mock/selectedMetrics";
import {saveDataToTheResultFile} from "./dev.utils";

const result1 = getAvailableMetricsFromDtoD(
    <any>dimensions_mock,
    <any>metrics_mock,
    <any>dimensionsMap_mock,
    <any>metricsMap_mock,
    selDims1,
    selectedMetrics1,
    false
);



saveDataToTheResultFile(result1);

