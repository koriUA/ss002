import Metric from './Metric';

export interface MetricsMap {
  [metricId: string]: Metric;
}
